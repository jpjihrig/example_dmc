# Example Deep Meta Classifier

I think it might confuse you when looking at my repo, so I just created some example files with the core of what I did. I might even adapt my scripts to this a bit, lets see. :)

## installation
1. Create a virtual environment.
2. ``` bash
   pip install -r requirements.txt
   ```
3. You should be able to run the scripts from this folder.

## create_dataset.py
The File `create_dataset.py` contains the definition of a simple classifier.
It should be good for MNIST, but also work for other small dataset as SVHN etc.

When run, it
1. trains some models per split
2. vectorizes them with the given method
3. saves them in a binary format

By doing so, it creates a folder with a train, test and validation split.

You would definitely need to
1. adjust the number of models per split
2. It only uses standard MNIST for training. You would need to integrate a dataset that has some property.
3. You would need to save the label for these models (I just saved the models in folders with the label name).

## deep_meta_classifier.py
The file `deep_meta_classifier.py` contains the definition of a variant of the deep meta classifier from the paper noted there. It should work to classify the rather small model used above (I think, I now even reduced the model size for the experiments I just want to run).

When run, the file
1. loads the saved model vectors from the splits
2. creates random labels (you need to replace this as mentioned above)
3. trains a deep meta classifier