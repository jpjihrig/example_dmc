import tensorflow as tf
from tensorflow.keras.layers import InputLayer, Conv1D, BatchNormalization, ReLU, MaxPool1D, Dense, Dropout, Input, Flatten
from typing import Tuple
from os import listdir, makedirs
from os.path import join
import numpy as np
from random import randint
from tqdm import tqdm

def ConvBlock(inputs, filters, pool=True):

    x = Conv1D(filters, 5)(inputs)
    x = ReLU()(x)
    x = BatchNormalization()(x)
    if pool:
        x = MaxPool1D(pool_size=2)(x)

    return x

def DenseBlock(inputs, units):

    x = Dense(units)(inputs)
    x = Dropout(rate=0.5)(x)
    x = ReLU()(x)
    x = BatchNormalization()(x)

    return x

def DeepMetaClassifier(inputs: Input, num_classes: int=2):
# This is the Deep Meta-Classifier (DMC) taken from https://arxiv.org/pdf/2002.05688.pdf
# I think there is one layer more here than in the paper.

    x = inputs
    for filters in [8, 16, 32, 64]:
        x = ConvBlock(x, filters)

    for i in range(5):
        x = ConvBlock(x, 128)

    x = ConvBlock(x, 128, pool=False)

    for i in range(5):
        x = ConvBlock(x, 256, pool=(i % 2 == 0))

    x = Flatten()(x)

    for i in range(4):
        x = DenseBlock(x, 1024)

    x = DenseBlock(x, 64)

    x = Dense(num_classes)(x)

    model = tf.keras.Model(inputs=inputs, outputs=x, name="DeepMetaClassifier")
    return model


if __name__ == '__main__':

    split_data = {
        'train': [],
        'validation': [],
        'test': [],
    }
    split_labels = {
    }

    # load model vectors
    # here, you would need to differentiate
    # between models with and without property
    for split in split_data.keys():
        folder = join(f'data/{split}')
        model_paths = [join(folder, file) for file in listdir(folder)]
        for path in tqdm(model_paths):
            split_data[split].append(np.load(path, allow_pickle=True))

    for split_name, data in split_data.items():
        # adjust data shape
        data = np.array(data)
        data = np.expand_dims(data, axis=-1)
        split_data[split_name] = data
        # create data labels, here only random
        # you would set the label respective to the dataset (no property/property)
        # the loaded classifier was trained with
        split_labels[split_name] = np.array([randint(0, 1) for i in range(len(data))])
        split_labels[split_name] = np.expand_dims(split_labels[split_name], axis=-1)

    # intput shape for model
    input_shape = split_data['train'][0].shape

    inputs = tf.keras.Input(shape=split_data['train'][0].shape)
    dmc = DeepMetaClassifier(inputs=inputs)
    dmc.compile(
        loss='sparse_categorical_crossentropy',
        optimizer='adam',
        metrics=['accuracy']
    )

    x_train = split_data['train']
    y_train = split_labels['train']

    dmc.fit(
        x=split_data['train'], y=split_labels['train'],
        epochs=2,
        batch_size=128,
        validation_data=(split_data['validation'], split_labels['validation']),
    )

    dmc.evaluate(x=split_data['test'], y=split_labels['test'])

    makedirs('models', exist_ok=True)
    dmc.save('models/dmc.h5')