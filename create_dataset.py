from os import makedirs
from os.path import join
import numpy as np
import tensorflow as tf
from tensorflow.keras import Sequential, Model
from tensorflow.keras.layers import BatchNormalization, Dense, Conv2D, MaxPool2D, Flatten
from tqdm import tqdm

def classifier_mnist() -> Model:
    """
    Classifier for MNIST.
    You can train this on MNIST/a modified version of MNIST and
    save the result together with no property/property label
    to create a data point.

    Returns:
        Model: Keras model to create train on MNIST and create data point.
    """
    model = Sequential()
    model.add(Conv2D(64, 3, activation='relu', input_shape=(28, 28, 1)))
    model.add(BatchNormalization())
    model.add(MaxPool2D())
    model.add(Conv2D(128, 3, activation='relu'))
    model.add(BatchNormalization())
    model.add(MaxPool2D())
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dense(10, activation='softmax'))
    model.summary()

    return model

def vectorize_model(model: tf.keras.Model, only_trainable: bool=True) -> np.array:
    """
    Create vector representation of given model
    following https://arxiv.org/pdf/2002.05688v1.pdf.

    Concatenate all trainable weights in the order in which they occur.
    The referenced paper only mentions saving trainable weights,
    therefore this is the default.

    Tested with keras Dense, Conv2D, BatchNorm and MaxPool layers.

    Args:
        model (Model): model to transform
            to vector representation

    Returns:
        np.array: vector of weights
    """

    vec = np.array([])

    for layer in model.layers:

        weights = layer.get_weights()

        if only_trainable and isinstance(layer, BatchNormalization):
            assert(len(weights) == 4)
            weights = weights[0:2]


        for w in weights:
            w = tf.reshape(w, [-1])
            vec = np.append(vec, w)

    return vec


def get_mnist_data():
    (x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()

    x_train = x_train.astype(np.float32) / 255.0
    x_test = x_test.astype(np.float32) / 255.0

    x_train = np.expand_dims(x_train, axis=-1)
    x_test = np.expand_dims(x_test, axis=-1)
    return (x_train, y_train), (x_test, y_test)


if __name__ == '__main__':

    (x_train, y_train), (x_test, y_test) = get_mnist_data()

    # I used ~ 2000, 400, 400
    splits = {
        'train': 2,
        'validation': 2,
        'test': 2
    }

    # In this example, I only use the normal mnist dataset
    # you would need to modify the dataset for half of the models with your property
    for split, times in splits.items():
        for i in tqdm(range(times)):
            m = classifier_mnist()
            m.compile(
                loss='sparse_categorical_crossentropy',
                optimizer='adam',
                metrics=['accuracy']
            )
            m.fit(
                x=x_train, y=y_train,
                epochs=2,
                batch_size=128,
                validation_data=(x_test, y_test),
            )
            vec = vectorize_model(model=m)

            # save model to split
            save_folder = f'data/{split}'
            makedirs(save_folder, exist_ok=True)
            np.save(file=join(save_folder, f'model_{i}.npy'), arr=vec)